#!/usr/bin/env node

const { execSync } = require('child_process');

const monitorOrder = [
    {
        name: 'eDP',
        isPrimary: true,
    },
    {
        name: 'HDMI-0',
    },
    {
        name: 'HDMI-A-0',
    },
    {
        name: 'DisplayPort-0',
    },
    {
        name: 'DisplayPort-1',
    },
    {
        name: 'DisplayPort-2',
    },
    {
        name: 'DisplayPort-3',
    },
    {
        name: 'DisplayPort-4',
    },
    {
        name: 'DisplayPort-5',
    },
    {
        name: 'DisplayPort-6',
    },
    {
        name: 'DisplayPort-7',
    },
    {
        name: 'DP-0',
        isPrimary: true,
    },
    {
        name: 'DP-1',
    },
    {
        name: 'DP-2',
    },
];

const commandToRun = ['xrandr'];

const monitors = [];

const result = execSync('xrandr');
const stdout = result.toString();
const lines = stdout.split('\n');

let shouldCheckMode = false;

for (const line of lines) {
    const result = line.match(/^(?<name>[-A-Za-z\d]+) (?<status>(dis)?connected)/);

    if (result) {
        shouldCheckMode = true;
        const { name, status } = result.groups;

        monitors.push({ name, status });
    } else if (shouldCheckMode) {
        shouldCheckMode = false;

        const modeResult = line.match(/^\s*(?<mode>\d+x\d+)/);

        if (modeResult) {
            const monitorStatus = monitors[monitors.length - 1];
            if (monitorStatus) {
                monitorStatus.mode = modeResult.groups.mode;
            }
        }
    }
}

let position = 0;

let wasPrimarySet = false;

for (const monitor of monitorOrder) {
    const { name, isPrimary } = monitor;

    const monitorStatus = monitors.find((m) => m.name === name);

    if (monitorStatus === undefined) {
        continue;
    }

    if (monitorStatus?.status === 'connected') {
        const [width] = monitorStatus.mode.split('x');

        commandToRun.push(
            `--output ${name}`,
            `--auto`,
            `${isPrimary && !wasPrimarySet ? '--primary' : ''}`,
            `--mode ${monitorStatus.mode}`,
            `--pos ${position}x0`
        );

        if (isPrimary && !wasPrimarySet) {
            wasPrimarySet = true;
        }

        position += +width;
    } else {
        commandToRun.push(`--output ${name} --off`);
    }
}

console.log(commandToRun.join(' '));
execSync(commandToRun.join(' '));

execSync('nitrogen --restore');
